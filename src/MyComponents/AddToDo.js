import React from 'react'
import { useState } from 'react';
export const AddToDo = (props) => {
    const [title, setTitle] = useState("");
    const [desc, setDesc] = useState("");
    const submit = (e) => {
        e.preventDefault();
        if (!title || !desc) {
            alert("title or desc cant be empty")
        }
        props.addTodo(title, desc);
        setTitle("");
        setDesc("");
    }
    return (
        <div>
            <h3>Add a ToDo</h3>
            <form onSubmit={submit
            }>
                <div className="mb-3">
                    <label htmlFor="title" className="htmlForm-label">To do Title</label>
                    <input type="text" value={title} onChange={(e) => { setTitle(e.target.value) }} className="htmlForm-control" id="title" />

                </div>
                <div className="mb-3">
                    <label htmlFor="desc" className="form-label">To do description</label>
                    <input type="text" value={desc} onChange={(e) => { setDesc(e.target.value) }} className="form-control" id="desc" />
                </div>
                <button type="submit" className="btn btn-primary">Add To Do</button>
            </form>
        </div>
    )
}
